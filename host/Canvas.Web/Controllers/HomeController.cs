﻿using Canvas.Application.Contracts;
using Canvas.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;

namespace Canvas.Web.Controllers
{
    public class HomeController : AbpController
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IAdService _adService;

        public HomeController(ILogger<HomeController> logger, IAdService adService)
        {
            _logger = logger;
            _adService = adService;
        }

        public async Task<IActionResult> Index()
        {
            await _adService.CreateAsync(new adDto() { Name = new Random().Next().ToString() });
            return View();
        }

        public async Task<IActionResult> Privacy()
        {
            return View(await _adService.GetAsync(1));
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
