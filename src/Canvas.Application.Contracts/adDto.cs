﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;

namespace Canvas.Application.Contracts
{
    public class adDto : EntityDto<int>
    {
        public string Name { get; set; }
    }

    public class ADQueryDto : PagedAndSortedResultRequestDto
    {
        
    }
}
