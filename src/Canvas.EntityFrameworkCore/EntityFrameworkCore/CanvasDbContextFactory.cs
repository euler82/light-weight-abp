﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Canvas.EntityFrameworkCore.EntityFrameworkCore
{
    public class CanvasDbContextFactory : IDesignTimeDbContextFactory<CanvasDbContext>
    {
        public CanvasDbContext CreateDbContext(string[] args)
        {
            var configuration = BuildConfiguration();

            var builder = new DbContextOptionsBuilder<CanvasDbContext>()
                .UseSqlServer(configuration.GetConnectionString("Default"));

            return new CanvasDbContext(builder.Options);
        }

        private static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);

            return builder.Build();
        }
    }
}
