﻿using Canvas.Domain;
using Canvas.Domain.Domain;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace Canvas.EntityFrameworkCore.EntityFrameworkCore
{
    [ConnectionStringName("Default")]
    public class CanvasDbContext : AbpDbContext<CanvasDbContext>
    {
        public DbSet<ad> ad { get; set; }
        public CanvasDbContext(DbContextOptions<CanvasDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ad>(b =>
            {
                b.ToTable(CanvasConsts.DbTablePrefix + "ad", CanvasConsts.DbSchema);
                b.ConfigureByConvention();
            });
        }
    }
}
